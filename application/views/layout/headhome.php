<!DOCTYPE html>
<html lang="en">
<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Colorlib">
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <!-- Page Title -->
    <title>StartOwl</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap.min.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <!-- Simple line Icon -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/simple-line-icons.css">
    <!-- Themify Icon -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/themify-icons.css">
    <!-- Hover Effects -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/set1.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/homepage.css">
</head>

<body>